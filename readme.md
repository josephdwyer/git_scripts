#Git Scrips  

## Requirements
* Git installed and and setup able to pull this repository.  
* If using windows, this is only tested using git bash.  

## Setup  
1. Clone this repository. ```git clone git@bitbucket.org:josephdwyer/git_scripts.git```  
1. Copy .bash_profile to your user's directory.  
  * Alternatively, copy the contents of the repository .bash_profile into your existing .bash_profile. ```cat git_scripts/.bash_profile >> ~/.bash_profile```  
1. Edit .bash_profile settings to your needs:  
  * ```GIT_SCRIPTS_DIR="/path/to/repo"``` - the location where you put the git_scripts repository.  
  * ```GIT_SCRIPTS_PROJECT_DIRECTORIES=("~/projects/" "path/to/other/projects")``` - places you keep git repositories you want the project command to be aware of.  
  * ```GIT_SCRIPTS_BACKUP_DIRECTORY="/path/to/backup/directory/"``` - directory you want the backup command to copy to.  
  * ```GIT_SCRIPTS_LOG_ENABLED=1``` - do you want to log the git_scripts commands you run, and their output.  
  * ```GIT_SCRIPTS_LOG_DIRECTORY="/path/to/log/directory/"``` - the directory in which to put the git_scripts logs.  
  * ```GIT_SCRIPTS_AUTO_UPDATE=1``` - do you want the git_scripts repo to be pulled when bash_profile is loaded?  
  * ```GIT_SCRIPTS_SPECIAL_BRANCHES=("master" "stage" "develop")``` - branches you don't want incremental commits on, and which are protected from the cleanup command.  
