# add this code to your .bash_profile in your home directory.

# load the git_scripts.
_loadGitScripts() {
  if [ -z "$GIT_SCRIPTS_AUTO_UPDATE" ]; then
    $GIT_SCRIPTS_AUTO_UPDATE = 1
  fi
  if [ -z "$GIT_SCRIPTS_DIR" ]; then
    $GIT_SCRIPTS_DIR = "~/git_scripts"
  fi
  if [ $GIT_SCRIPTS_AUTO_UPDATE -eq 1 ]; then
    local currentDir=$PWD
    cd $GIT_SCRIPTS_DIR
    # for a novice user we may want to force them to be on master...
    #git checkout master && git pull
    # for development and/or customisation of these scripts, we could allow any branch.
    git pull
    cd $currentDir
  fi
  source "$GIT_SCRIPTS_DIR/git_scripts.bash"
  #source ~/.bashrc
}

##
# Git Scripts user settings.
#

# directory containing the git_scripts repository.
GIT_SCRIPTS_DIR=~/git_scripts #c:/projects/git_scripts

# directories you keep git repositories in.
# project command will search in these directories in order, taking the first one found.
GIT_SCRIPTS_PROJECT_DIRECTORIES=("/Volumes/code/" "~/") #("c:/projects/" "c:/inetpub/wwwroot/")

# used by backup command.
GIT_SCRIPTS_BACKUP_DIRECTORY="$(printf "%s/backup/" ~)"

# do you want to log the script commands you run?
GIT_SCRIPTS_LOG_ENABLED=$TRUE
GIT_SCRIPTS_LOG_DIRECTORY="$(printf "%s/git_log/" ~)"

# when .bash_profile loads, do you want to pull the latest changes to the git_scripts?
GIT_SCRIPTS_AUTO_UPDATE=1

# special branches will not allow using the 'save' command, and won't be removed by cleanup
GIT_SCRIPTS_SPECIAL_BRANCHES=("master" "stage" "develop")

_loadGitScripts
