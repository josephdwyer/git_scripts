##
# Constants
#

TRUE=1
FALSE=0
SUCCESS=0
ERROR=1

WINDOWS="WindowsNT"
LINUX="Linux"
MAC="Darwin"

##
# System Settings
#

##
# OS Settings
#
OS="`uname`"
if [ "$OS" == "$WINDOWS" ]; then
  #PATHSEPERATOR="\\"
  EXPLORERCMD="explorer"
elif [ "$OS" == "$LINUX" ]; then
  #PATHSEPERATOR="/"
  EXPLORERCMD="nautilus"
elif [ "$OS" == "$MAC" ]; then
  #PATHSEPERATOR="/"
  EXPLORERCMD="open"
fi

##
# User Settings
#

if [ -z "$GIT_SCRIPTS_SPECIAL_BRANCHES" ]; then
  GIT_SCRIPTS_SPECIAL_BRANCHES=("master" "stage" "develop")
fi

if [ -z "$GIT_SCRIPTS_PROJECT_DIRECTORIES" ]; then
  GIT_SCRIPTS_PROJECT_DIRECTORIES=("~/")
fi

if [ -z "$GIT_SCRIPTS_LOG_ENABLED" ]; then
  GIT_SCRIPTS_LOG_ENABLED=$TRUE
fi

if [ -z "$GIT_SCRIPTS_LOG_DIRECTORY" ]; then
  GIT_SCRIPTS_LOG_DIRECTORY="$(printf "%s/git_log/" ~)"
fi

if [ -z "$GIT_SCRIPTS_BACKUP_DIRECTORY" ]; then
  GIT_SCRIPTS_BACKUP_DIRECTORY="$(printf "%s/backup/" ~)"
fi

# color options are available: http://www.arwin.net/tech/bash.php
# override these with .bashrc
if [ -z "$GIT_SCRIPTS_COMMAND_COLOR" ]; then
  GIT_SCRIPTS_COMMAND_COLOR='\e[1;36m'
fi
if [ -z "$GIT_SCRIPTS_PROMPT_COLOR" ]; then
  GIT_SCRIPTS_PROMPT_COLOR='\e[1;31m'
fi
if [ -z "$GIT_SCRIPTS_ERROR_COLOR" ]; then
  GIT_SCRIPTS_ERROR_COLOR='\e[1;31m'
fi
endColor='\e[0m'


##
# General Functions
#

##
# Reload the .bash_profile and .bashrc
#
reload() {
  source ~/.bash_profile
  if [ $? -ne $SUCCESS ]; then
    printf "bash_profile error!\n"
  else
    # success the bashrc loaded message should show
    printf "bash_profile reloaded\n"
  fi
  source ~/.bashrc
  if [ $? -ne $SUCCESS ]; then
    printf "bashrc error!\n"
  else
    # success the bashrc loaded message should show
    printf "bashrc reloaded\n"
  fi
}

##
# output command text and command output to screen and log to file.
#
_logExecute() {
  printf "${GIT_SCRIPTS_COMMAND_COLOR}$1${endColor}\n"
  _log "$1"
  # execute command $1, pass the std output to tee which will _log to the file LOG_FILENAME
  local CMD="$1 2>&1 | tee -a $LOG_FILENAME"
  # eval clobbers the return status, so we declare a new array a and set it equal to the PIPESTATUS
  eval "$CMD; declare -a a=(\${PIPESTATUS[@]})"
  # the user's real command was the first one, so return that value from this function.
  local RV=${a[0]}
  # move to next line if they did not
  _log ""
  return $RV
}

##
# log to file and output to screen using error color
#
_logError() {
  printf "${GIT_SCRIPTS_ERROR_COLOR}$1${endColor}\n"
  _log "** ERROR: $1"
}

##
# Log to file (do not display to user)
#
_log() {
  if [ $GIT_SCRIPTS_LOG_ENABLED -eq $TRUE ]; then
    if [ ! -d $GIT_SCRIPTS_LOG_DIRECTORY ]; then
      mkdir $GIT_SCRIPTS_LOG_DIRECTORY
    fi
    local logFileName="$GIT_SCRIPTS_LOG_DIRECTORY$(date +%Y%m%d).log"
    local TEXT="$1"'\n'
    echo -e $TEXT >>$logFileName;
  fi
}

##
# check if the given array contains a value.
#
_arrayContains () {
  local e
  declare -a arr=("${!2}")

  for e in ${arr[@]}; do
    if [ "$e" = "$1" ]; then
      return $TRUE
    fi
  done
  return $FALSE
}

##
# determine if the git repository contains any files with conflicts.
#
_hasConflicts() {
  # check for merge conflicts.
  local conflictFiles=($(git diff --name-only --diff-filter=U))
  if [ ${#conflictFiles[@]} -gt 0 ]; then
    _logError "Merge conflict detected. Once the merge is resolved, try $FUNCNAME again"
    echo "$TRUE"
    return $TRUE
  fi
  echo "$FALSE"
  return $FALSE
  # both echo and return this so it may be used either way $(_hasConflicts) or _hasConflicts $?
}

##
# get the name of the branch that is currently active.
#
_getCurrentBranch() {
  local ref=$(git symbolic-ref HEAD 2> /dev/null) || exit 0
  local branchName="${ref#refs/heads/}"
  echo "$branchName"
}

##
# determine if there are any changes that are able to be stashed.
#
_hasStashableChanges() {
  local rtrn=$FALSE
  # staged uncommitted changes
  git diff-index --quiet --cached HEAD
  if [ $? -ne 0 ]; then
    rtrn=$TRUE
  fi

  # unstaged changes
  git diff-files --quiet
  if [ $? -ne 0 ]; then
    rtrn=$TRUE
  fi

  # untracked & unignored
  local u="$(git ls-files --exclude-standard --others)" && test -z "$u"
  if [ -n "$u" ]; then
    rtrn=$TRUE
  fi

  # both echo and return this so it may be used either way $(_hasStashableChanges) or _hasStashableChanges $?
  echo "$rtrn"
  return $rtrn
}

##
# _ask the user a question, and make sure you get an answer you are looking for.
# _ask "Can you answer my question?" ("Yes" "No") $userResult
#
_ask() {
  local question="$1"
  declare -a askAnswers=("${!2}")
  local userInput=""

  _log $question

  printf "$GIT_SCRIPTS_PROMPT_COLOR%s (%s)$endColor\n" "$question" "${askAnswers[*]}"

  while true; do
    read userInput
    _log $userInput
    _arrayContains "$userInput" askAnswers[@]
    if [ $? -eq $TRUE ]; then
      break
    else
      printf "${GIT_SCRIPTS_ERROR_COLOR}Please enter one of (%s). %s $endColor\n" "${askAnswers[*]}" "$question"
      _log $question
    fi
  done
  # a bit funky, but this is how to return a string.
  local __ask_result_var=$3
  eval $__ask_result_var="'$userInput'"
}

##
# check to make sure a function is being called with the correct number of arguments
# print usage statement if not.
#
_usage() {
  # check the _usage of the _usage function.
  if [ $# -gt 3 ]; then
    printf "${GIT_SCRIPTS_ERROR_COLOR}usage:\n\t%s\t%s${endColor}\n" "$FUNCNAME" "$(_get_usageParams \"$FUNCNAME\")"
    return $ERROR
  fi

  local requiredParamCount=$1
  local functionName=$2
  declare -a argsParam=("${!3}")
  local paramCount=${#argsParam[@]}

  # _log the function call and arguments.
  _log "### $(date): $functionName ${argsParam[@]}"
  if [ $paramCount -ne $requiredParamCount ]; then
    local funcParams="$(_get_usageParams "$functionName")"
    printf "${GIT_SCRIPTS_ERROR_COLOR}usage:\n\t%s\t%s${endColor}\n" "$functionName" "$funcParams"
    return $ERROR
  fi
  return $SUCCESS
}

##
# print the parameters of a function.
#
_get_usageParams() {
  # git bash (on windows) does not support bash 4, so cannnot use associative array, this is a crude workaround.
  local funcName=$1
  if [ "_usage" = "$funcName" ]; then
    printf "requiredParamCount functionName params"
  elif [ "project" = "$funcName" ]; then
    printf "nameOfProjectFolder"
  elif [ "reload" = "$funcName" ]; then
    printf ""
  elif [ "files" = "$funcName" ]; then
   printf  ""
  elif [ "work-on" = "$funcName" ]; then
    printf "branchName"
  elif [ "start-on" = "$funcName" ]; then
    printf "newBranchName"
  elif [ "get-remote-branch" = "$funcName" ]; then
    printf "remoteBranchName"
  elif [ "save" = "$funcName" ]; then
    printf "\"message describing commit\""
  elif [ "update-from" = "$funcName" ]; then
    printf "nameOfBranchToUpdateFrom"
  elif [ "check-into" = "$funcName" ]; then
    printf "nameOfBranchToSendChangesTo"
  elif [ "finish" = "$funcName" ]; then
    printf ""
  elif [ "cleanup" = "$funcName" ]; then
    printf ""
  elif [ "branch-diff" = "$funcName" ]; then
    printf "mode(all|files|log) nameOfOtherBranch"
  elif [ "backup" = "$funcName" ]; then
    printf "fileName-you-want-to-backup"
  else
    printf "(No parameter information found.)"
  fi
  printf "\n"
}
# this is used to setup autocompletion
declare -a autoCompleteWithBranches=("work-on" "check-into" "update-from" "get-remote-branch")

##
# Print the help statement for a function.
#
_get_usageHelp() {
  local funcName=$1
  if [ "_usage" = "$funcName" ]; then
    printf ""
  elif [ "project" = "$funcName" ]; then
    printf "Try to navigate to the folder in one of the project directories."
  elif [ "reload" = "$funcName" ]; then
    printf "Reload the .bash_profile and .bashrc files. Use if you have made changes to either of these files."
  elif [ "files" = "$funcName" ]; then
    printf "Open file browser in the current directory."
  elif [ "work-on" = "$funcName" ]; then
    printf "Switch branches. Identical to 'git checkout branchName'. "
  elif [ "start-on" = "$funcName" ]; then
    printf "Create a new branch with the supplied name. The branch will be created from whatever branch you are on when you call start-on."
  elif [ "get-remote-branch" = "$funcName" ]; then
    printf "Get a local copy of an existing remote branch."
  elif [ "branch-diff" = "$funcName" ]; then
    printf "Display the differences between the current branch and the branch specified. Use this to view what the result of a check-into would do. The format of the diff depends on the mode ('all' use the difftool, 'files' show only the file names, 'log' show the commits)."
  elif [ "save" = "$funcName" ]; then
    printf "Commits changes added to the index, via 'git add' or 'git rm' and push the changes to the remote branch."
  elif [ "update-from" = "$funcName" ]; then
    printf "Get changes committed on the branch specified."
  elif [ "check-into" = "$funcName" ]; then
    printf "Move changes from this branch to the branch specified."
  elif [ "finish" = "$funcName" ]; then
    printf "Remove this branch from your computer or the remote."
  elif [ "cleanup" = "$funcName" ]; then
    printf "Experimental, remove branches from this computer that are no longer on the remote. This function is not thoroughly tested."
  elif [ "backup" = "$funcName" ]; then
    printf "copy the file specified to the backup directory and append a timestamp (only one file at a time, no funny business)."
  else
    printf "(No help text found.)"
  fi
  printf "\n"
}

##
# prints help for all functions
#
githelp() {
  declare -a displayHelpFor=("project" "files" "*"
    "get-remote-branch" "start-on" "work-on" "save" "update-from" "check-into" "branch-diff" "finish" "*"
    "cleanup" "backup")

  for itm in "${displayHelpFor[@]}"; do
    if [ "$itm" = "*" ]; then
      printf "$GIT_SCRIPTS_ERROR_COLOR**********************************\n$endColor"
    else
      local itmParams="$(_get_usageParams "$itm")"
      local itmHelp="$(_get_usageHelp "$itm")"
      printf "$GIT_SCRIPTS_COMMAND_COLOR%s\t%s\n$endColor" $itm "$itmParams"
      printf "\t%s\n\n" "$itmHelp"
    fi
  done
}


##
# open the file browser in the current directory.
#
files() {
  $EXPLORERCMD .
}

##
# change to the project specified
#
project() {
  local args=("$@")
  _usage 1 "$FUNCNAME" args[@]
  if [ $? -ne $SUCCESS ]; then
    return $ERROR
  fi

  for file in "${GIT_SCRIPTS_PROJECT_DIRECTORIES[@]}"
  do
    #cd "$file$1"
    if [ -d "$file$1" ]; then
      cd "$file$1"
      return $SUCCESS
    fi
  done
  _logError "Could not find directory '$1' in project directories (${GIT_SCRIPTS_PROJECT_DIRECTORIES[@]})."
  return $ERROR
}

##
# display differences between branches. (as if you were going to check-into that branch)
#
branch-diff() {
  # generic check _usage.
  local args=("$@")
  _usage 2 "$FUNCNAME" args[@]
  if [ $? -ne $SUCCESS ]; then
    return $ERROR
  fi
  local mode=$1
  local targetBranchName=$2
  local startBranchName="$(_getCurrentBranch)"
  #if [[ $branchName != origin* ]]; then
  #  _logError "Please reference a remote branch, A.K.A. the branch name needs to start with 'origin'"
  #  return $ERROR
  #fi

  if [ $mode = "all" ]; then
    _logExecute "git difftool HEAD $targetBranchName"
  elif [ $mode = "files" ]; then
    _logExecute "git diff --name-status $targetBranchName"
  elif [ $mode = "log" ]; then
    _logExecute "git log $targetBranchName..$startBranchName"
  else
    _logError "Unknown diff type."
    return $ERROR
  fi
}


##
# get a branch that exists remotely such that you can interact with it.
# use for getting an existing branch, or a branch that someone else created.
# this is no longer needed, git checkout handles this case correctly now.
#
get-remote-branch() {
  # generic check _usage.
  local args=("$@")
  _usage 1 "$FUNCNAME" args[@]
  if [ $? -ne $SUCCESS ]; then
    return $ERROR
  fi
  # fill in variables with parameters.
  local  targetBranchName=$1
  if [[ $targetBranchName != origin* ]]; then
    _logError "Please reference a remote branch, A.K.A. the branch name needs to start with 'origin'"
    return $ERROR
  fi
  local localBranchName=${targetBranchName#"origin/"}

  _logExecute "git checkout -b $localBranchName $targetBranchName"
  if [ $? -ne $SUCCESS ]; then
    _logError "Checkout failed."
    return $ERROR
  fi
}


##
# start-on
#	example _usage:
#		start-on new-feature-name
#	creates a branch (locally and remote) named new-feature-name
#
start-on() {
  # generic check _usage.
  local args=("$@")
  _usage 1 "$FUNCNAME" args[@]
  if [ $? -ne $SUCCESS ]; then
    return $ERROR
  fi
  # fill in variables with parameters.
  local  targetBranchName=$1

  local startBranchName="$(_getCurrentBranch)"
	_log "Current Branch: $startBranchName"


  # fetch any changes from remote.
	_logExecute "git fetch --all"

  # stash any changes that are in the working directory if necessary.
  local doStash="$(_hasStashableChanges)"
  if [ $doStash -eq $TRUE ]; then
    _logExecute "git stash save --include-untracked --no-keep-index \"$FUNCNAME $(date)\""
    if [ $? -ne $SUCCESS ]; then
      _logError "Status: $targetBranchName not created."
      _logError "Error creating stash. This is odd. Do you have unmerged files? fix the issue and call $FUNCNAME again."
      return $ERROR
    fi
  fi

  # get the latest from origin.
  _logExecute "git pull origin $startBranchName"
  if [ $? -ne $SUCCESS ]; then
    _logError "Status: $targetBranchName not created."
    _logError "Updating from remote failed. Resolve the issue before proceding. Then call $FUNCNAME again."
    if [ $doStash -eq $TRUE ]; then
      _logError "Changes in your working directory were stashed use 'git stash pop' to restore them."
    fi
    return $ERROR
  fi

  # check for merge conflicts.
  if [ "$(_hasConflicts)" -eq "$TRUE" ]; then
    _logError "Status: $startBranchName update in progress. $targetBranchName not created."
    _logError "Pull generated a merge conflict. Fix the merge conflict and commit the result. Then call $FUNCNAME again."
    if [ $doStash -eq $TRUE ]; then
      _logError "Changes in your working directory were stashed use 'git stash pop' to restore them."
    fi
    return $ERROR
  fi

  # restore stashed changes if necessary.
  if [ $doStash -eq $TRUE ]; then
    _logExecute "git stash pop"
    if [ $? -ne $SUCCESS ]; then
      _logError "Status: $startBranchName updated. $targetBranchName not created."
      _logError "Restoring stash failed. There are probably merge conflicts. Resolve the issue and call $FUNCNAME again."
      return $ERROR
    fi
  fi

  # create branch locally.
	_logExecute "git checkout -b $targetBranchName"
	if [ $? -ne $SUCCESS ]; then
    _logError "Status: $startBranchName updated. $targetBranchName not created."
		_logError "Checkout failed. Resolve the issue and call $FUNCNAME again."
		return $ERROR
	fi

  # push branch to remote.
	_logExecute "git push --set-upstream origin $targetBranchName"
	if [ $? -ne $SUCCESS ]; then
    _logError "Status: $startBranchname updated. $targetBranchName created."
		_logError "Create branch on remote failed. Fix the issue and call 'git push origin $targetBranchName'"
		return $ERROR
	fi
} # end start-on

##
# work-on switches to specified branch.  Identical to manually using `git checkout branch-name`
# historically, this function did more.
#
work-on() {
  _logError "Command disabled. use git checkout branch-name instead."
  if [ $FALSE -eq $TRUE ]; then
    local args=("$@")
    _usage 1 "$FUNCNAME" args[@]
    if [ $? -ne $SUCCESS ]; then
      return $ERROR
    fi
    local targetBranchName=$1

    _logExecute "git checkout $targetBranchName"
    if [ $? -ne $SUCCESS ]; then
      _logError "Branch ('$targetBranchName') checkout failed. Aborting work-on (no changes made)."
      return $ERROR
    fi
  fi
}

##
# commit your changes locally, and back them up on your branch on the remote
# note: does not add files. use git add for that.
#	example _usage:
#		save 'changing wording on dialog'
#
save() {
  # generic check _usage.
  local args=("$@")
  _usage 1 "$FUNCNAME" args[@]
  if [ $? -ne $SUCCESS ]; then
    return $ERROR
  fi
	local commitMessage=$1

  local currentBranch="$(_getCurrentBranch)"
	_log "Current Branch: $currentBranch"

  _arrayContains "$currentBranch" GIT_SCRIPTS_SPECIAL_BRANCHES[@]
	if [ $? -eq $TRUE ]; then
    _logError "Error! you are on a special branch (${GIT_SCRIPTS_SPECIAL_BRANCHES[*]}) Save will not work. Perhaps if you want incremental backup of your code you should create a branch (use the start-on command)."
		return $ERROR
	else

		if [ -n "$(git ls-files --exclude-standard --others)" ]; then
			_logExecute "git status"
			#_logError "It looks like you have some untracked files.\nRemember, save does not add new files use 'git add path/to/filename.extension' followed by save to add a file.\nCheck 'git status' for the exact files."
      _logError "It looks like you have some new files.\nSave does not add new files, use 'git add pat/to/fileName.ext'"

      local answers=("yes" "no")
      local userAnswer=""
      _ask "Would you like to continue saving without the untracked files?" answers[@] userAnswer

      if [ "$userAnswer" = "no" ]; then
        _logError "Save aborted."
        return $ERROR
      fi
		fi

		_logExecute "git commit -a -m \"$commitMessage\""
		if [ $? -ne $SUCCESS ]; then
			_logError "Commit failed. Aborting save (files not committed)."
			return $ERROR
		fi

		_logExecute "git push origin $currentBranch"
		if [ $? -ne $SUCCESS ]; then
			_logError "Push failed. Aborting save (files committed locally, changes not pushed to remote)."
			return $ERROR
		fi

	fi
}



##
# update the current branch by first updating from chosen branch, and merging the changes
#
update-from() {
  # generic check _usage.
  local args=("$@")
  _usage 1 "$FUNCNAME" args[@]
  if [ $? -ne $SUCCESS ]; then
    return $ERROR
  fi

	local targetBranchName=${1}
  local startBranchName="$(_getCurrentBranch)"
	_log "Start Branch: $startBranchName"

  # don't let update from origin/ branches, it will result in a detatched head...
  if [[ $targetBranchName = origin* ]]; then
    _logError "Please do not reference a remote branch, use 'get-remote-branch $targetBranchName' then 'git checkout $startBranchname' and '$FUNCNAME ${targetBranchName#"origin/"}'"
    return $ERROR
  fi

	_logExecute "git fetch --all"

  # stash any changes that are in the working directory if necessary.
  local doStash="$(_hasStashableChanges)"
  if [ $doStash -eq $TRUE ]; then
    _logExecute "git stash save --include-untracked --no-keep-index \"$FUNCNAME $(date)\""
    if [ $? -ne $SUCCESS ]; then
      _logError "Error creating stash. This is odd. Do you have unmerged files? fix the issue and call $FUNCNAME again."
      return $ERROR
    fi
  fi

  # switch to the branch we are after.
	_logExecute "git checkout $targetBranchName"
	if [ $? -ne $SUCCESS ]; then
		_logError "Checkout failed. Aborting update (no changes made)."
    if [ $doStash -eq $TRUE ]; then
      _logError "Changes in your working directory were stashed use 'git stash pop' to restore them."
    fi
    return $ERROR
	fi

  # get the latest from origin.
  _logExecute "git pull origin $targetBranchName"
  if [ $? -ne $SUCCESS ]; then
    _logError "Updating from remote failed. Resolve the issue before proceding. Then call $FUNCNAME again."
    if [ $doStash -eq $TRUE ]; then
      _logError "Changes in your working directory were stashed use 'git stash pop' to restore them."
    fi
    return $ERROR
  fi

  # check for merge conflicts.
  if [ "$(_hasConflicts)" -eq "$TRUE" ]; then
    _logError "Status: $targetBranchName update in progress."
    _logError "Pull generated a merge conflict. Fix the merge conflict ('git mergetool') and commit the result ('save'). Then call $FUNCNAME again."
    if [ $doStash -eq $TRUE ]; then
      _logError "Changes in your working directory were stashed use 'git stash pop' to restore them."
    fi
    return $ERROR
  fi

  # switch back to the starting branch.
	_logExecute "git checkout $startBranchName"
	if [ $? -ne $SUCCESS ]; then
		_logError "Checkout failed. Aborting update ($targetBranchName has been updated, $startBranchName has not)."
		return $ERROR
	fi

  # merge the branch in.
	_logExecute "git merge $targetBranchName"
	if [ $? -ne $SUCCESS ]; then
		_logError "Merge problem. You need to manually finish merging."
    if [ $doStash -eq $TRUE ]; then
      _logError "Changes in your working directory were stashed use 'git stash pop' to restore them."
    fi
		return $ERROR
	fi

  # restore stashed changes if necessary.
  if [ $doStash -eq $TRUE ]; then
    _logExecute "git stash pop"
    if [ $? -ne $SUCCESS ]; then
      _logError "Restoring stash failed. There are probably merge conflicts. Resolve the issue and call $FUNCNAME again."
      return $ERROR
    fi
  fi
}

##
# merge your changes into the chosen branch and push them to bitbucket
#
check-into() {
  # generic check _usage.
  local args=("$@")
  _usage 1 "$FUNCNAME" args[@]
  if [ $? -ne $SUCCESS ]; then
    return $ERROR
  fi

	local targetBranchName=${1}

  local startBranchName="$(_getCurrentBranch)"
	_log "Start Branch: $startBranchName"

  # don't let update from origin/ branches, it will result in a detatched head...
  if [[ $targetBranchName = origin* ]]; then
    _logError "Please do not reference a remote branch, use 'get-remote-branch $targetBranchName' then 'git checkout $startBranchname' and '$FUNCNAME ${targetBranchName#"origin/"}'"
    return $ERROR
  fi
	_logExecute "git fetch --all"

	# 1) warn if there are any untracked files
	if [ -n "$(git ls-files --exclude-standard --others)" ]; then
		git status
		_logError "It looks like you have some untracked files.\nRemember, save does not add new files use 'git add path/to/filename.extension' followed by save to add a file.\nCheck 'git status' for the exact files."

    local answers=("yes" "no")
    local userAnswer=""
    _ask "Do you want to continue with the checkin (not including untracked files)?" answers[@] userAnswer
    if [ "$userAnswer" == "no" ]; then
      _logError "$FUNCNAME cancelled"
      return $ERROR
    fi
	fi

	# 2) warn if there are any uncommitted, modified files
	git diff-files --quiet
	if [ $? -ne $SUCCESS ]; then
		git status
		_logError "It looks like you have some uncommitted changes.\nRemember, checkin only moves changes that have been committed. Use the 'save' command to commit any changes you have made."
    local answers=("yes" "no")
    local userAnswer=""
    _ask "Do you want to continue with the checkin (not including uncommitted changes)?" answers[@] userAnswer
    if [ "$userAnswer" == "no" ]; then
      _logError "checkin cancelled"
      return $ERROR
    fi
	fi

  # stash any changes that are in the working directory if necessary.
  local doStash="$(_hasStashableChanges)"
  if [ $doStash -eq $TRUE ]; then
    _logExecute "git stash save --include-untracked --no-keep-index \"$FUNCNAME $(date)\""
    if [ $? -ne $SUCCESS ]; then
      _logError "Error creating stash. This is odd. Do you have unmerged files? fix the issue and call $FUNCNAME again."
      return $ERROR
    fi
  fi

  # switch to the branch we are after.
	_logExecute "git checkout $targetBranchName"
	if [ $? -ne $SUCCESS ]; then
		_logError "Checkout failed. Aborting $FUNCNAME (no changes made)."
    if [ $doStash -eq $TRUE ]; then
      _logError "Changes in your working directory were stashed use 'git stash pop' to restore them."
    fi
    return $ERROR
	fi

  # get the latest from origin.
  _logExecute "git pull origin $targetBranchName"
  if [ $? -ne $SUCCESS ]; then
    _logError "Updating from remote failed. Resolve the issue before proceding. Then call $FUNCNAME again."
    if [ $doStash -eq $TRUE ]; then
      _logError "Changes in your working directory were stashed use 'git stash pop' to restore them."
    fi
    return $ERROR
  fi

  # check for merge conflicts.
  if [ "$(_hasConflicts)" -eq "$TRUE" ]; then
    _logError "Status: $targetBranchName update in progress."
    _logError "Pull generated a merge conflict. Fix the merge conflict and commit the result. Then call $FUNCNAME again."
    if [ $doStash -eq $TRUE ]; then
      _logError "Changes in your working directory were stashed use 'git stash pop' to restore them."
    fi
    return $ERROR
  fi

  # check to see if the merge will generate a conflict...
  #git format-patch $(git merge-base $targetBranchName $startBranchName)..$startBranchName --stdout | git apply --check
	#if [ $? -ne $SUCCESS ]; then
  #  _logError "Merging would generate a merge conflict."
  #  _logExecute "git checkout $startBranchName"
  #  _logError "You must merge $targetBranchName into your branch first, try the 'update' command, or 'git merge'. Aborting $FUNCNAME ($targetBranchName updated)."
  #  return $ERROR
  #fi


  # do the merge.
  _logExecute "git merge --squash $startBranchName"
	#_logExecute "git merge --no-ff $startBranchName"
	if [ $? -ne $SUCCESS ]; then
    _logError "Error merging. Most likely merging would generate a merge conflict."
    _logExecute "git reset --hard"
    _logExecute "git checkout $startBranchName"
    _logError "You must merge $targetBranchName into your branch first, try the 'update-from' command, or 'git merge $targetBranchName'. Aborting $FUNCNAME ($targetBranchName updated)."
    if [ $doStash -eq $TRUE ]; then
      _logError "Changes in your working directory were stashed use 'git stash pop' to restore them."
    fi
		##_logExecute "git checkout $startBranchName"
		#_logError "Error! merging $startBranchName into $targetBranchName."
    #_logError "If there is a merge conflict, call 'git mergetool' resolve the merge conflicts then commit the result and 'git push origin $targetBranchName'"
		return $ERROR
	fi

  # commit the result
  git status
  printf "(Please include any ticket numbers if applicable.)\nWhat did you do on this branch?"
  local squashMessage=""
  read squashMessage
  _logExecute "git commit -m \"$squashMessage\""
  if [ $? -ne $SUCCESS ]; then
    _logError "Commit failed. Aborting $FUNCNAME (files not committed)."
    if [ $doStash -eq $TRUE ]; then
      _logError "Changes in your working directory were stashed use 'git stash pop' to restore them."
    fi
    return $ERROR
  fi

  # push the results to the remote
	_logExecute "git push origin $targetBranchName"
	if [ $? -ne $SUCCESS ]; then
		_logError "Pushing changes to remote failed. Aborting $FUNCNAME (changes have been moved from your branch '$startBranchName' to $targetBranchName, but not pushed to the remote)."
    if [ $doStash -eq $TRUE ]; then
      _logError "Changes in your working directory were stashed use 'git stash pop' to restore them."
    fi
		return $ERROR
	fi

  # change back to the starting point.
  _logExecute "git checkout $startBranchName"
  if [ $? -ne $SUCCESS ]; then
    _logError "Checkout failed. Aborting $FUNCNAME."
    if [ $doStash -eq $TRUE ]; then
      _logError "Changes in your working directory were stashed use 'git stash pop' to restore them."
    fi
    return $ERROR
  fi

  # merge the branch we just checked into, because we created a new commit on it
  # if we were to go back to committing on this branch, we would have a merge conflict when we chekin again.
  _logExecute "git merge $targetBranchName"
  if [ $? -ne $SUCCESS ]; then
    _logError "Merging committed state back into working branch failed. Consider deleting this branch and creating a new one to avoid merge conflicts if you keep working in this branch."
    if [ $doStash -eq $TRUE ]; then
      _logError "Changes in your working directory were stashed use 'git stash pop' to restore them."
    fi
    return $ERROR
  fi

  # restore stashed changes if necessary.
  if [ $doStash -eq $TRUE ]; then
    _logExecute "git stash pop"
    if [ $? -ne $SUCCESS ]; then
      _logError "Restoring stash failed. There are probably merge conflicts. Resolve the issue and call $FUNCNAME again."
      return $ERROR
    fi
  fi

}

##
# an update in git (~1.7) has taken away the usefulness of this function.
# "git branch -d branch" used to refuse deleting the branch even when
# the branch is fully merged to its upstream branch if it is not merged
# to the current branch.  It now deletes it in such a case.
# the purpose of this function was to make sure the changes had made it into master before deleting.
# it needs to be re-written.
#
finish() {
  _logError "Command disabled. Use git branch -d branch-name and git push origin --delete branch-name."
  if [ $FALSE -eq $TRUE ]; then
    # generic check _usage.
    local args=("$@")
    _usage 0 "$FUNCNAME" args[@]
    if [ $? -ne $SUCCESS ]; then
      return $ERROR
    fi
    local startBranchName="$(_getCurrentBranch)"
    _log "Start Branch: $startBranchName"

    _logExecute "git fetch --all"

    _logExecute "git checkout master"
    if [ $? -ne $SUCCESS ]; then
      _logError "Checkout failed. Aborting finished (no changes have been made)."
      return $ERROR
    fi

    local answers=("yes" "no")
    local userAnswer=""
    _ask "Delete the local branch?" answers[@] userAnswer
    if [ "$userAnswer" == "yes" ]; then
      _logExecute "git branch -d $startBranchName"
      if [ $? -ne $SUCCESS ]; then
        userAnswer=""
        _ask "Changes not pushed to remote. Delete anyway?" answers[@] userAnswer
        if [ "$userAnswer" == "yes" ]; then
          _logExecute "git branch -D $startBranchName"
        elif [ "$userAnswer" == "no" ]; then
          _logError "Delete branch failed. Aborting $FUNCNAME (no changes have been made)."
          return $ERROR
        fi
      fi
    elif [ "$userAnswer" == "no" ]; then
      # don't delete local branch.
      _log "Local branch not deleted."
    fi

    userAnswer=""
    _ask "Do you want do delete the remote branch?" answers[@] userAnswer
    if [ "$userAnswer" == "yes" ]; then
      _logExecute "git push origin :$startBranchName"
      if [ $? -ne $SUCCESS ]; then
        _logError "Deleting remote branch failed. Aborting $FUNCNAME (Local branch has been deleted)."
        return $ERROR
      fi
    elif [ "$userAnswer" == "no" ]; then
      _log "Remote branch not deleted."
    fi
  fi
}

##
# Prune the remote branches, and delete local branches that are merged
#
cleanup() {
	# todo: check if there are any changes. warn about it.

  local startBranchName="$(_getCurrentBranch)"
	_log "Current Branch: $startBranchName"

  # stash any changes that are in the working directory if necessary.
  local doStash="$(_hasStashableChanges)"
  if [ $doStash -eq $TRUE ]; then
    _logExecute "git stash save --include-untracked --no-keep-index \"$FUNCNAME $(date)\""
    if [ $? -ne $SUCCESS ]; then
      _logError "Error creating stash. This is odd. Do you have unmerged files? fix the issue and call $FUNCNAME again."
      return $ERROR
    fi
  fi

## this file goes in your home directory
# e.g. ~/ or C:\Users\jdwyer\.bashrc

  _logExecute"git fetch"
	_logExecute "git checkout master"
	_logExecute "git remote prune origin"
	printf "${GIT_SCRIPTS_ERROR_COLOR}Delete these (merged) branches?${endColor}\n"

  local branchNames=$(printf "\|%s" "${GIT_SCRIPTS_SPECIAL_BRANCHES[@]}")
	git branch --merged | grep -v "\*${branchNames}"

  local answers=("yes" "no")
  local userAnswer=""
  _ask "Delete these (merged) branches?" answers[@] userAnswer
  if [ "$userAnswer" == "yes" ]; then
    printf "${GIT_SCRIPTS_ERROR_COLOR}Deleting${endColor}\n"
    git branch -d `git branch --merged | grep -v "\*${branchNames}" | xargs`
  elif [ "$userAnswer" == "no" ]; then
    _logError "Branches not deleted."
  fi

  _logExecute "git checkout $startBranchName"
	if [ $? -ne $SUCCESS ]; then
		_logError "Checkout failed."
    if [ $doStash -eq $TRUE ]; then
      _logError "Changes in your working directory were stashed use 'git stash pop' to restore them."
    fi
    return $ERROR
	fi

  # restore stashed changes if necessary.
  if [ $doStash -eq $TRUE ]; then
    _logExecute "git stash pop"
    if [ $? -ne $SUCCESS ]; then
      _logError "Restoring stash failed. There are probably merge conflicts."
      return $ERROR
    fi
  fi
}

##
# open the gitk tool for recovering lost commits.
#
recover-commit() {
  gitk --all $( git fsck --no-reflog | awk '/dangling commit/ {print $3}' )
}

##
# copy the file to a backup directory and append the date to the end of the file name.
# note: decision made for possible conflicts during daylight savings time transition (same time will happen twice) for the sake of easy reading of backup times.
#
backup() {
  if [ ! -d $GIT_SCRIPTS_BACKUP_DIRECTORY ]; then
    mkdir $GIT_SCRIPTS_BACKUP_DIRECTORY
  fi
  if [ ! -d $GIT_SCRIPTS_BACKUP_DIRECTORY$PWD ]; then
    mkdir -p "$GIT_SCRIPTS_BACKUP_DIRECTORY$PWD"
  fi
  cp "$PWD/$1" "$GIT_SCRIPTS_BACKUP_DIRECTORY$PWD"
  mv "$GIT_SCRIPTS_BACKUP_DIRECTORY$PWD/$1" "$GIT_SCRIPTS_BACKUP_DIRECTORY$PWD/$1.$(date +%Y-%m-%d_%H-%M-%S)"
}

##
# Clear out all of the ASP .NET temp files, and restart IIS.
#
clear-net-temp() {
  iisreset.exe /STOP
  printf ""

  printf "x32 2.0"
  rm -rf c:/Windows/Microsoft.NET/Framework/v2.0.50727/Temporary\ ASP.NET\ Files/*
  printf " ... Done\n"

  printf "x32 4.0"
  rm -rf c:/Windows/Microsoft.NET/Framework/v4.0.30319/Temporary\ ASP.NET\ Files/*
  printf " ... Done\n"

  printf "x64 2.0"
  rm -rf c:/Windows/Microsoft.NET/Framework64/v2.0.50727/Temporary\ ASP.NET\ Files/*
  printf " ... Done\n"

  printf "x64 4.0"
  rm -rf c:/Windows/Microsoft.NET/Framework64/v4.0.30319/Temporary\ ASP.NET\ Files/*
  printf " ... Done\n"

  iisreset.exe /START
  printf "\n"
}



#####
# Auto complete for the commands
#####

##
# auto complete branch names.
#
__branch_name_comp() {
	local cur prev opts
	cur="${COMP_WORDS[COMP_CWORD]}"
  prev="${COMP_WORDS[COMP_CWORD-1]}"
  local rlst=`__git_refs`

	local IFS=$'\n'
	COMPREPLY=($(compgen -W "${rlst}" -- "${cur}"))
}

##
# auto complete folders in project directories.
#
__project_name_comp() {
  local cur prev opts
  cur="${COMP_WORDS[COMP_CWORD]}"
  prev="${COMP_WORDS[COMP_CWORD-1]}"
  local currentDir=$PWD

  local completes=""
  for file in "${GIT_SCRIPTS_PROJECT_DIRECTORIES[@]}"
  do
    if [ -d $file ]; then
      cd $file
      # todo? filter to only directories - find ./ -type d -maxdepth 1
      local lf=`ls -A`
      completes="$completes $lf"
    fi
  done
  cd $currentDir
  local IFS=$'\n'
  COMPREPLY=($(compgen -W "${completes}" -- "${cur}"))
}

##
# autocomplete list used for branch-diff command.
#
__branch-diff_comp() {
  local cur prev opts
  cur="${COMP_WORDS[COMP_CWORD]}"
  prev="${COMP_WORDS[COMP_CWORD-1]}"
  local rlst=`__git_refs`
  local completes="all files log $rlst"
  COMPREPLY=($(compgen -W "${completes}" -- "${cur}"))
}

for itm in "${autoCompleteWithBranches[@]}"; do
  complete -o bashdefault -o default -o nospace -F __branch_name_comp $itm 2>/dev/null || complete -o default -o nospace -F __branch_name_comp $itm
done

# project
complete -o bashdefault -o default -o nospace -F __project_name_comp project 2>/dev/null \
	|| complete -o default -o nospace -F __project_name_comp project

# branch-diff
complete -o bashdefault -o default -o nospace -F __branch-diff_comp branch-diff 2>/dev/null \
  || complete -o default -o nospace -F __branch-diff_comp branch-diff


printf 'git_script loaded.\n'
